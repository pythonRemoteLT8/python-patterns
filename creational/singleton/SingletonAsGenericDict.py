class GenDict:

    _shared_state = {}
    def __init__(self):
        self.__dict__ = self._shared_state
        print("The common instance dict: %s " % self.__dict__ )

class Singleton(GenDict):

    def __init__(self, arg):
        GenDict.__init__(self)
        self.val = arg
        print("The common instance dict after singleton: %s " % self.__dict__ )

    def __str__(self):
        return self.val

def main():

    x = Singleton('apple')
    print(x)

    y = Singleton('pear')
    #print(y)

    z = Singleton('plum')

    #w = Singleton('grape')

    print(x)
    print(y)
    print(z)
    #print(w)


if __name__ == '__main__':
    main()