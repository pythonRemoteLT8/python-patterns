class Singleton:

    class __Singleton:
        def __init__(self):
            self.val = None

        def __str__(self):

            if not self.val is None:
                return repr(self) + self.val

    __instance = None

    def __new__(cls):

        if not Singleton.__instance:
            print("The common instance is not created yet: %s " % "None")

            Singleton.__instance = Singleton.__Singleton()

        #print("The common instance is created: %s " % cls.__instance )
        return Singleton.__instance

def main():

    x = Singleton()
    x.val = 'test01'
    print(x)

    y = Singleton()
    print(y)
    y.val = 'test02'
    print(y)

if __name__ == '__main__':
    main()