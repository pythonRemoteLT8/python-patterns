class Singleton:
    __instance = None

    @classmethod
    def get_instance(cls):
        if not cls.__instance:
            cls.__instance = Singleton()

        print("The common instance is created: %s " % cls.__instance )
        return cls.__instance

    def __init__(self):

        if not self.__instance:
            print("The common instance has not been created: %s " % self.__instance )
        self.val = 0

    def function(self):
        print(self.val)

def main():

    a = Singleton.get_instance()
    a.function()
    a.val = 10
    a.function()

    b = Singleton.get_instance()
    b.function()


if __name__ == '__main__':
    main()