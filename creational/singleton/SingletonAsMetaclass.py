class SingletonType(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            print("The common instance has not been created: %s " % cls)

            cls._instances[cls] = super(SingletonType, cls).__call__(*args, **kwargs)
            print("The common instance has been created: %s " % cls._instances[cls] )

        print("The common instance is: %s " % cls._instances[cls])
        return cls._instances[cls]

# If class names do not match
class SingletonClass(metaclass=SingletonType):
    pass

# If class names do not match
class SingletonClassForApples(metaclass=SingletonType):
    pass

# If class names match
class SingletonClassForOranges(metaclass=SingletonType):
    pass

def main():

    x = SingletonClassForApples()
    y = SingletonClassForOranges()

    z = SingletonClass()
    w = SingletonClass()

    print( "x.__class__ == y.__class__ : %s " % (x == y))

    print( "z.__class__ == w.__class__ : %s " % (z == w))

if __name__ == '__main__':
    main()

