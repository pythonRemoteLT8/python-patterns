
def Singleton(class_):

    __instances = {}

    def get_instance(*args, **kwargs):
        if class_ not in __instances:
            print("The common instance is not created yet: %s " % "None")
            __instances[class_] = class_(*args, **kwargs)

        print("The common instance has been created: %s " % __instances[class_])
        return __instances[class_]
    return get_instance

@Singleton
class FirstClassForApples:
    def __init__(self):
        self.val = 0

@Singleton
class SecondClassForOranges:
    def __init__(self):
        self.val = "Orange"

def main():
    a = FirstClassForApples()
    print(a.val)
    a.val = 10
    b = FirstClassForApples()
    print(b.val)

    c = SecondClassForOranges()
    print(c.val)
    c.val = "More oranges"
    d = SecondClassForOranges()
    print(d.val)

if __name__ == '__main__':
    main()