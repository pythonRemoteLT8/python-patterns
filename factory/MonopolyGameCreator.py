from factory.GameFactory import GameFactory
from factory.BoardGame import BoardGame

class MonopolyGameCreator(GameFactory):
    def create(self):
        return BoardGame("Monopoly", "Family Game", 4)