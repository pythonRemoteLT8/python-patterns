from factory.Game import Game

class PCGame(Game):

    def __init__(self, name, type, min_player_num, max_player_num, is_online):
        self._name = name
        self._type = type
        self._min_player_num = min_player_num
        self._max_player_num = max_player_num
        self._is_online = is_online

    def get_name(self):
        return self._name

    def get_type(self):
        return self._type

    def get_min_number_of_players(self):
        return self._min_player_num

    def get_max_number_of_players(self):
        return self._max_player_num

    def can_be_played_remotely(self):
        return False

    def __str__(self):
        return (f"{__name__} [name='{self._name}', type='{self._type}', min_player_num={self._min_player_num}"
                f", max_player_num={self._max_player_num}, is_online={self._is_online}]")