from factory.GameFactory import GameFactory
from factory.PCGame import PCGame

class ValorantGameCreator(GameFactory):
    def create(self):
        return PCGame("Valorant", "FPS", 4, 10, True)