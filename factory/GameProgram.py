from ValorantGameCreator import *
from MonopolyGameCreator import *

def main():
    game_type = input('Enter the type of game [PC, Board]: ')
    game_factory = None
    if game_type == 'PC':
        game_factory = ValorantGameCreator()
    elif game_type == 'Board':
        game_factory = MonopolyGameCreator()

    if game_factory:
        game = game_factory.create()
        print(game)


if __name__ == '__main__':
    main()