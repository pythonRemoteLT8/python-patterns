from Game import *

class BoardGame(Game):
    def __init__(self, name, type, max_player_num):
        self._name = name
        self._type = type
        self._max_player_num = max_player_num

    def get_name(self):
        return self._name

    def get_type(self):
        return self._type

    def get_min_number_of_players(self):
        return 2

    def get_max_number_of_players(self):
        return self._max_player_num

    def can_be_played_remotely(self):
        return False

    def __str__(self):
        return f"{__name__} [name='{self._name}', type='{self._type}', max_player_num={self._max_player_num}]"