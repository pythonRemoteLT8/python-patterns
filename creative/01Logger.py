import datetime
import time

class Logger:
    class __Singleton:
        def __init__(self, file_name):
            self._file_name = file_name

        def log(self, log_msg):
            with open(self._file_name, 'a') as file:
                file.write(f'{datetime.datetime.now()}: {log_msg}\n')

    __instance = None

    ## Construct a Singleton
    def __new__(cls, file_name):

        ## If that thing is initialized
        if not Logger.__instance:

            ## IF not: (not initialized yet) - then initialize
            Logger.__instance = Logger.__Singleton(file_name)

        ## Just return the instance which is initialized
        return Logger.__instance

def main():

    print("Logger created as singleton")
    logger = Logger('test.log')
    logger.log('message 1')
    time.sleep(1)
    logger.log('message 2')
    time.sleep(1.5)
    logger.log('message 3')

    logger1 = Logger('')
    logger1.log('Write new message')

if __name__ == '__main__':
    main()