class Sedan:
    def who_am_i(self):
        pass

class LHDSedan(Sedan):
    def who_am_i(self):
        pass

class CarFactory:
    def create_sedan(self):
        pass

    def create_station_wagon(self):
        pass

    def create_coupe(self):
        pass


class LHDCarFactory(CarFactory):
    def create_sedan(self):
        pass

    def create_station_wagon(self):
        pass

    def create_coupe(self):
        pass