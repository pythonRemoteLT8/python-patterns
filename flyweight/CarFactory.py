from flyweight.Car import Car
from flyweight.Engine import Engine


class CarFactory:
    engines = [Engine('polo', 1.6, 'DIESEL'),
               Engine('poloGTI', 2.0, 'GASOLINE'),
               Engine('golf', 1.5, 'GASOLINE'),
               Engine('e', 0.0, 'ELECTRIC')]

    @staticmethod
    def create_vw_polo(vin):
        return Car('VW', vin, 'Polo', CarFactory.engines[0])

    @staticmethod
    def create_vw_polo_gti(vin):
        return Car('VW', vin, 'Polo GTI', CarFactory.engines[1])

    @staticmethod
    def create_vw_golf(vin):
        return Car('VW', vin, 'Golf', CarFactory.engines[2])

    @staticmethod
    def create_skoda_citigo(vin):
        return Car('Skoda', vin, 'CityGO', CarFactory.engines[3])