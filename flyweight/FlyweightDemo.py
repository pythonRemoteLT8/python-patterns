from random import *

from flyweight.CarFactory import CarFactory
from flyweight.Engine import Engine


def main():
    produced_cars = []
    for i in range(1000):
        engine_type = randint( 0, 3 )
        if engine_type == 0:
            produced_cars.append(CarFactory.create_vw_polo(randint(1000000000, 9999999999)))
        elif engine_type == 1:
            produced_cars.append(CarFactory.create_vw_polo_gti(randint(1000000000, 9999999999)))
        elif engine_type == 2:
            produced_cars.append(CarFactory.create_vw_golf(randint(1000000000, 9999999999)))
        else:
            produced_cars.append(CarFactory.create_skoda_citigo(randint(1000000000, 9999999999)))

    print(f"I created {len(produced_cars)}, but only {Engine.instances} references to Engine objects")


if __name__ == '__main__':
    main()