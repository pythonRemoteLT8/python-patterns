from decorator.FragStatistics import FragStatistics


class DisplayCountersDecorator(FragStatistics):
    def __init__(self, frag_statistics):
        self._frag_statistics = frag_statistics

    def increment_frag_count(self):
        frag_count = self._frag_statistics.increment_frag_count()
        print(f"Your frag count is: {frag_count}")
        return frag_count

    def increment_death_count(self):
        death_count = self._frag_statistics.increment_death_count()
        print(f"Your death count is: {death_count}")
        return death_count

    def reset(self):
        self._frag_statistics.reset()
        print("Stats reset - KDR is equal to 0")