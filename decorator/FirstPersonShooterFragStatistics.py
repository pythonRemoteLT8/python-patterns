from decorator.FragStatistics import FragStatistics


class FirstPersonShooterFragStatistics(FragStatistics):
    def __init__(self):
        self._frags_count = 0
        self._death_count = 0

    def increment_frag_count(self):
        self._frags_count += 1
        return self._frags_count

    def increment_death_count(self):
        self._death_count += 1
        return self._death_count

    def reset(self):
        self._frags_count = 0
        self._death_count = 0