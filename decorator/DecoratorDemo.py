from decorator.DeathCountInfoDecorator import DeathCountInfoDecorator
from decorator.DisplayCounterDecorator import DisplayCountersDecorator
from decorator.FirstPersonShooterFragStatistics import FirstPersonShooterFragStatistics
from decorator.FragDeathRatioDecorator import FragDeathRatioDecorator
from decorator.FragInfoDecorator import FragInfoDecorator


def main():

    statistics = FirstPersonShooterFragStatistics()

    statistics.increment_death_count()  # nothing will appear on the screen
    statistics.increment_frag_count()   # nothing will appear on the screen

    decorated_statistics = FragDeathRatioDecorator(FragInfoDecorator(DisplayCountersDecorator(DeathCountInfoDecorator(statistics))))

    decorated_statistics.increment_frag_count()
    decorated_statistics.increment_frag_count()
    decorated_statistics.increment_frag_count()
    decorated_statistics.increment_death_count()

if __name__ == '__main__':
    main()