from decorator.FragStatistics import FragStatistics


class DeathCountInfoDecorator(FragStatistics):
    def __init__(self, frag_statistics):
        self._frag_statistics = frag_statistics

    def increment_frag_count(self):
        return self._frag_statistics.increment_frag_count()

    def increment_death_count(self):
        print('Fragged by an enemy')
        return self._frag_statistics.increment_death_count()

    def reset(self):
        self._frag_statistics.reset()