from decorator.FragStatistics import FragStatistics


class FragDeathRatioDecorator(FragStatistics):
    
    def __init__(self, frag_statistics):
        self._frag_statistics = frag_statistics
        self._current_frag_count = None
        self._current_death_count = None

    def increment_frag_count(self):
        self._current_frag_count = self._frag_statistics.increment_frag_count()
        self._display_frag_deaths_ratio()
        return self._current_frag_count

    def increment_death_count(self):
        self._current_death_count = self._frag_statistics.increment_death_count()
        self._display_frag_deaths_ratio()
        return self._current_death_count

    def _display_frag_deaths_ratio(self):
        if self._current_frag_count and self._current_death_count:
            print(f'KDR is {self._current_frag_count/self._current_death_count}')

    def reset(self):
        self._frag_statistics.reset()