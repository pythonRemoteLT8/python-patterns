class Cook:
    '''
    Director - manages the creation of the object
    '''
    def __init__(self):
        self._builder = None

    def prepare(self, builder):
        self._builder = builder
        self._builder.prepare_dough()
        self._builder.add_extras()
        self._builder.bake()

class PizzaBuilder:
    '''
    Builder - abstract interface for creating target objects
    '''
    def __init__(self):
        self.pizza = Pizza()

    def prepare_dough(self): pass

    def add_extras(self): pass

    def bake(self): pass

class MargeritaBuilder(PizzaBuilder):
    '''
    ConcreteBuilder - a specific builder that creates and combines the components of the created object
    '''
    def prepare_dough(self): pass

    def add_extras(self): pass

    def bake(self): pass

class PepperoniBuilder(PizzaBuilder):
    def prepare_dough(self): pass

    def add_extras(self): pass

    def bake(self): pass

class Pizza:
    '''
    the generated complex object
    '''
    pass

def main():
    cook = Cook()
    # we choose a builder
    baking = PepperoniBuilder()
    cook.prepare(baking)
    pizza = baking.pizza

    print( pizza )

if __name__ == '__main__':
    main()

### The task is to construct 2 different builders for different pizzas:
#  * PepperoniBuilder
#  * MargeritaBuilder

#  Using Cook as Director bake 2 different pizzas so that the object pizza
#  displays : <__main__.MargeritaPizza object at 0x7f6b326f8970> and
#  <__main__.PepperoniPizza object at 0x986e316f8971>


