from bridge.Drink import Drink


class Coffee(Drink):
    def get_volume(self):
        return "150ml"

    def is_addictive(self):
        return True

    def get_number_of_sugar_lumps(self):
        return 0

    def get_taste(self):
        return "bitter"