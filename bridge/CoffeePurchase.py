from bridge.Coffee import Coffee
from bridge.DrinkPurchase import DrinkPurchase


class CoffeePurchase(DrinkPurchase):
    def buy(self, cost):
        print(f"Buying a coffee for ${cost}")
        return Coffee()