from bridge.DrinkPurchase import DrinkPurchase
from bridge.Tea import Tea


class TeaPurchase(DrinkPurchase):
    def buy(self, cost):
        print(f"Buying a tea for ${cost}")
        return Tea()