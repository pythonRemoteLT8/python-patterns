
from bridge.CoffeePurchase import CoffeePurchase
from bridge.DrinkPurchase import DrinkPurchase
from bridge.TeaPurchase import TeaPurchase


def main():

    drink = DrinkPurchase()
    drink.buy( 1.00 )

    coffee = CoffeePurchase()
    coffee.buy( 2.29 )
    print( coffee )

    tea = TeaPurchase()
    tea.buy( 1.40 )
    print( tea )

if __name__ == '__main__':
    main()