from bridge.Drink import Drink


class Tea(Drink):
    def get_volume(self):
        return "250ml"

    def is_addictive(self):
        return False

    def get_number_of_sugar_lumps(self):
        return 2

    def get_taste(self):
        return "sweet"