from adapter.as_objects.Student import Student


class FavoriteAdapter(Student):
    def __init__(self, Favorite):
        self._Favorite = Favorite

    def get_full_name(self):
        return self._Favorite.get_first_name() + " " + self._Favorite.get_last_name()

    def get_contact_details(self):
        return self._Favorite.get_email()

    def is_adult(self):
        return self._Favorite.get_age() >= 18

    def get_results(self):
        return self._Favorite.get_grades()