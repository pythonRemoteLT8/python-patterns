from adapter.as_objects.Favorite import Favorite
from adapter.as_objects.FavoriteAdapter import FavoriteAdapter


def main():
    students = [FavoriteAdapter(Favorite('Steven', 'Morgan', 'sm@gmail.com', 19, [3, 4, 5])),
                FavoriteAdapter(Favorite('Maria', 'Smith', 'mk@hotmail.com', 17, [4, 5, 4])),
                FavoriteAdapter(Favorite('Joanna', 'Noris', 'jn@yahoo.com', 21, [2, 4, 6]))]

    for s in students:
        print(f"{'Adult' if s.is_adult() else 'Child'} {s.get_full_name()} [{s.get_contact_details()}]: {s.get_results()}")


if __name__ == '__main__':
    main()