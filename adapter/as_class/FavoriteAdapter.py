from adapter.as_class.Favorite import Favorite
from adapter.as_class.Student import Student


class FavoriteAdapter(Student, Favorite):
    
    def __init__(self, first_name, last_name, email, age, grades):
        super().__init__(first_name, last_name, email, age, grades)

    def get_full_name(self):
        return self.get_first_name() + " " + self.get_last_name()

    def get_contact_details(self):
        return self.get_email()

    def is_adult(self):
        return self.get_age() >= 18

    def get_results(self):
        return self.get_grades()