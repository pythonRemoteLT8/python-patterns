class StackInterface:

    def push(self, data):
        pass

    def pop(self):
        pass

    def is_empty(self):
        pass

class Stack(StackInterface):

    def __init__(self):
        self._data = []

    def push(self, data):
        self._data.append(data)

    def pop(self):
        return self._data.pop()

    def is_empty(self):
        return not self._data


def main():
    expr = '2 7 + 3 / 14 3 - 4 * + 2 /'
    #expr = '12 2 3 4 * 10 5 / + * +'
    #expr = '5 1 2 + 4 * + 3 -'
    stack = Stack()

    for el in expr.split():
        if el.isnumeric():
            stack.push(float(el))
        else:
            a = stack.pop()
            b = stack.pop()
            if el == '+':
                stack.push(a + b)
            elif el == '-':
                stack.push(b - a)
            elif el == '*':
                stack.push(a * b)
            else:
                stack.push(b / a)

    print(stack.pop())
    print(stack.is_empty())


if __name__ == '__main__':
    main()