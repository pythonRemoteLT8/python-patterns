from orca.braille import Line
from composite.Point import Point

class CompoundLine(Line):
    def __init__(self):
        self._lines = []

    def draw(self, length):
        for line in self._lines:
            line.draw(length)

    @property
    def starting_position(self):
        if not self._lines:
            return Point(0, 0)
        else:
            return self._lines[0].starting_position()

    @starting_position.setter
    def starting_position(self, value):
        for line in self._lines:
            line.starting_position = value

    def add_line(self, line):
        self._lines.append(line)

    def remove_line(self, line):
        self._lines.remove(line)