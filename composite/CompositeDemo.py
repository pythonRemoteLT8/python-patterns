from composite.CompoundLine import CompoundLine
from composite.DottedLine import DottedLine
from composite.SolidLine import SolidLine


def main():
    dotted_1 = DottedLine()
    dotted_1.starting_position = (1, 1)

    dotted_2 = DottedLine()
    dotted_2.starting_position = (2, 2)

    solid_1 = SolidLine()
    solid_1.starting_position = (3, 3)

    solid_2 = SolidLine()
    solid_2.starting_position = (4, 4)

    compound_1 = CompoundLine()
    compound_2 = CompoundLine()

    # folding a tree structure
    compound_1.add_line(dotted_1)
    compound_1.add_line(solid_1)
    compound_1.add_line(compound_2)
    compound_2.add_line(dotted_2)
    compound_2.add_line(solid_2)

    # common interface for node and leaf
    compound_2.starting_position = (5, 5)

    solid_2.starting_position = (6, 6)

    compound_1.draw(5)


if __name__ == '__main__':
    main()