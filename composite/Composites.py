class Element:
    def get_name(self):
        pass

    def get_salary(self):
        pass

    def get_vacation_days(self):
        pass


class Employee(Element):
    def __init__(self, name, salary, vacation_days):
        self._name = name
        self._salary = salary
        self._vacation_days = vacation_days

    def get_name(self):
        return self._name

    def get_salary(self):
        return self._salary

    def get_vacation_days(self):
        return self._vacation_days


class Departament(Element):
    def __init__(self, name):
        pass

    def get_name(self):
        pass

    def get_salary(self):
        pass

    def get_vacation_days(self):
        pass

    def add_element(self, element):
        pass