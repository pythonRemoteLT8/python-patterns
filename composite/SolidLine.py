from composite.Line import Line
from composite.Point import Point


class SolidLine(Line):
    
    def __init__(self):
        self._point = Point(0, 0)

    def draw(self, length):
        print(f"Drawing solid line starting in {self._point} with lenght {length}")

    @property
    def starting_position(self):
        return self._point

    @starting_position.setter
    def starting_position(self, value):
        self._point = value