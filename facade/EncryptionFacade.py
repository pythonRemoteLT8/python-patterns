from facade.Encryptors import Encryptors


class EncryptionFacade(Encryptors):

    def __init__(self, scrypt_encryptor, bcrypt_encryptor, noop_encryptor):
        self._scrypt_encryptor = scrypt_encryptor
        self._bcrypt_encryptor = bcrypt_encryptor
        self._noop_encryptor = noop_encryptor

    def encrypt_without_modification(self, to_encrypt):

        print( "NoCrypt: %s" % self._noop_encryptor )
        return self._noop_encryptor.encrypt(to_encrypt)

    def encrypt_with_bcrypt(self, to_encrypt):

        print( "BCrypt: %s" % self._bcrypt_encryptor )
        return self._bcrypt_encryptor.encrypt(to_encrypt)

    def encrypt_with_scrypt(self, to_encrypt):

        print("SCrypt: %s" % self._scrypt_encryptor)
        return self._scrypt_encryptor.encrypt(to_encrypt)