class OrderFacade:
    
    def __init__(self, delivery_service, payment_service, product_availability_service):
        self._delivery_service = delivery_service
        self._payment_service = payment_service
        self._product_availability_service = product_availability_service

    def place_order(self, product_id, amount, recipient):
        if self._product_availability_service.is_available(product_id):
            self._payment_service.pay(product_id, amount)
            self._delivery_service.deliver_product(product_id, amount, recipient)