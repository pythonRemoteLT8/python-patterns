from facade.Encryptor import Encryptor
import pyscrypt

class SCryptEncryptor(Encryptor):

    def __init__(self):
        self._salt = b'aa1f2d3f4d23ac44e9c5a6c3d8f9ee8c'

    def encrypt(self, to_encrypt):

        hashed = pyscrypt.hash( to_encrypt.encode("utf-8"), self._salt, 2048, 8, 1, 32 )
        return f"encrypting {to_encrypt} with SCrypt %s" % hashed.hex()
