from facade.Encryptor import Encryptor


class NoOpEncryptor(Encryptor):
    def encrypt(self, to_encrypt):
        return to_encrypt