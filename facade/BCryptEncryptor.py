from facade.Encryptor import Encryptor
import bcrypt

class BCryptEncryptor(Encryptor):

    def __init__(self):
        self._salt = bcrypt.gensalt()

    def encrypt(self, to_encrypt):

        hashed = bcrypt.hashpw( to_encrypt.encode("utf-8"), self._salt)
        return f"encrypting {to_encrypt} with BCrypt %s" % hashed