from facade.BCryptEncryptor import BCryptEncryptor
from facade.EncryptionFacade import EncryptionFacade
from facade.NoOpEncryptor import NoOpEncryptor
from facade.SCryptEncryptor import SCryptEncryptor

def main():

    scrypt_encryptor = SCryptEncryptor()
    bcrypt_encryptor = BCryptEncryptor()
    noop_encryptor = NoOpEncryptor()

    encryptorFacade = EncryptionFacade( scrypt_encryptor, bcrypt_encryptor, noop_encryptor )

    print(  encryptorFacade.encrypt_with_bcrypt( "One" ) )
    print(  encryptorFacade.encrypt_with_scrypt( "Two" ) )
    print(  encryptorFacade.encrypt_without_modification( "Three" ) )

if __name__ == '__main__':
    main()