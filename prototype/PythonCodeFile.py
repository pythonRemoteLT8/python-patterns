import copy


class PythonCodeFile:
    def __init__(self, license_content, file_extension, code='', file_name='' ):
        self._license_content = license_content
        self._code = code
        self._file_name = file_name
        self._file_extension = file_extension

    @property
    def license_content(self):
        return self._license_content

    @license_content.setter
    def license_content(self, value):
        self._license_content = value

    @property
    def code(self):
        return self._code

    @code.setter
    def code(self, value):
        self._code = value

    @property
    def file_name(self):
        return self._file_name

    @file_name.setter
    def file_name(self, value):
        self._file_name = value

    @property
    def file_extension(self):
        return self._file_extension

    @file_extension.setter
    def file_extension(self, value):
        self._file_extension = value

    def create_clone(self):
        return copy.copy(self)

    def __str__(self):
        return f"File: {self._file_name}.{self._file_extension}, license=[{self._license_content}], code=[{self._code}]"