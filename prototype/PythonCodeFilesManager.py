from prototype.PythonCodeFile import PythonCodeFile


class PythonCodeFilesManager:
    _base_file = PythonCodeFile('SDA', 'py')

    @staticmethod
    def create_file_with_content(file_name, code):
        base_file_clone = PythonCodeFilesManager._base_file.create_clone()
        base_file_clone.file_name = file_name
        base_file_clone.code = code
        return base_file_clone


def main():
    file_1 = PythonCodeFilesManager.create_file_with_content('zen_of_python', 'import this')
    file_2 = PythonCodeFilesManager.create_file_with_content('test_file', 'test')

    print(file_1)
    print(file_2)


if __name__ == '__main__':
    main()