from proxy.DiskImage import DiskImage
from proxy.ImageProxy import ImageProxy
from proxy.InternetImage import InternetImage

def main():
    disk_image_proxy = ImageProxy(DiskImage())
    internet_image_proxy = ImageProxy(InternetImage())
                                # the user only uses the Image interface
    disk_image_proxy.show()     # the proxy only loads the image when it is needed
                                # the picture from the Internet is never loaded


if __name__ == '__main__':
    main()