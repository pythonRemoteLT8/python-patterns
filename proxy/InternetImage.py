from proxy.RemoteImage import RemoteImage


class InternetImage(RemoteImage):
    def load_from_source(self):
        pass

    def is_loaded(self):
        pass

    def show(self):
        pass