from proxy.Image import Image


class ImageProxy(Image):
    def __init__(self, remote_image):
        self._remote_image = remote_image

    def show(self):

        if not self._remote_image.is_loaded():

            print( "Show: %s" % self._remote_image )
            self._remote_image.load_from_source()

        print("Show: %s" % self._remote_image.show() )
        return self._remote_image.show()