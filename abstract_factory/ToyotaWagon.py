from abstract_factory.Toyota import ToyotaCorolla


class ToyotaCorollaWagon(ToyotaCorolla):
    def get_type(self):
        return "Wagon"

    def get_cylinders_num(self):
        return 4

    def get_engine_volume(self):
        return 1.6

    def get_trunk_size(self):
        return 540