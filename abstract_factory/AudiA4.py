from abstract_factory.AbstractCar import AbstractCar


class AudiA4(AbstractCar):
    def get_model_name(self):
        return "A4"

    def get_producer(self):
        return "Audi"