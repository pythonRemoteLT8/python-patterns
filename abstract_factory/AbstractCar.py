from abstract_factory.Car import Car


class AbstractCar(Car):
    def __str__(self):
        return (f"Car: {self.get_producer()} {self.get_model_name()} {self.get_type()} has {self.get_cylinders_num()} "
                f"cylinders and engine {self.get_engine_volume()} and trunk size {self.get_trunk_size()} litres")