from abstract_factory.AbstractCar import AbstractCar


class ToyotaCorolla(AbstractCar):
    def get_model_name(self):
        return "Corolla"

    def get_producer(self):
        return "Toyota"