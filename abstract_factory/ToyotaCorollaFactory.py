from abstract_factory.CarFactory import CarFactory
from abstract_factory.ToyotaHatchback import ToyotaCorollaHatchback
from abstract_factory.ToyotaSedan import ToyotaCorollaSedan
from abstract_factory.ToyotaWagon import ToyotaCorollaWagon


class ToyotaCorollaFactory(CarFactory):
    def create_Wagon(self):
        return ToyotaCorollaWagon()

    def create_hatchback(self):
        return ToyotaCorollaHatchback()

    def create_sedan(self):
        return ToyotaCorollaSedan()