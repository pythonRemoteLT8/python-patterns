from abstract_factory.AudiA4 import AudiA4


class AudiA4Wagon(AudiA4):
    def get_type(self):
        return "Wagon"

    def get_cylinders_num(self):
        return 4

    def get_engine_volume(self):
        return 1.8

    def get_trunk_size(self):
        return 520