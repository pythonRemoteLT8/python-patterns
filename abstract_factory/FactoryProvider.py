from abstract_factory.AudiA4Factory import AudiA4Factory
from abstract_factory.ToyotaCorollaFactory import ToyotaCorollaFactory


class FactoryProvider:
    @staticmethod
    def create_factory(factory_type):
        if factory_type == 'T':
            return ToyotaCorollaFactory()
        elif factory_type == 'A':
            return AudiA4Factory()
        else:
            return None

def main():
    factory_type = input('What car do you want to produce - choose A or T: ')
    factory = FactoryProvider.create_factory(factory_type)

    if factory:
        hatchback = factory.create_hatchback()
        print(hatchback)

if __name__ == '__main__':
    main()