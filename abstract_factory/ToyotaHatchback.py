from abstract_factory.Toyota import ToyotaCorolla


class ToyotaCorollaHatchback(ToyotaCorolla):
    def get_type(self):
        return "Hatchback"

    def get_cylinders_num(self):
        return 4

    def get_engine_volume(self):
        return 2.0

    def get_trunk_size(self):
        return 350