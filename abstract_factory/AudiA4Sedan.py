from abstract_factory.AudiA4 import AudiA4


class AudiA4Sedan(AudiA4):
    def get_type(self):
        return "Sedan"

    def get_cylinders_num(self):
        return 4

    def get_engine_volume(self):
        return 2.0

    def get_trunk_size(self):
        return 450