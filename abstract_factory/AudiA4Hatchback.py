from abstract_factory.AudiA4 import AudiA4


class AudiA4Hatchback(AudiA4):
    def get_type(self):
        return "Hatchback"

    def get_cylinders_num(self):
        return 6

    def get_engine_volume(self):
        return 2.4

    def get_trunk_size(self):
        return 300