from abstract_factory.AudiA4Hatchback import AudiA4Hatchback
from abstract_factory.AudiA4Sedan import AudiA4Sedan
from abstract_factory.AudiA4Wagon import AudiA4Wagon
from abstract_factory.CarFactory import CarFactory


class AudiA4Factory(CarFactory):
    def create_Wagon(self):
        return AudiA4Wagon()

    def create_hatchback(self):
        return AudiA4Hatchback()

    def create_sedan(self):
        return AudiA4Sedan()