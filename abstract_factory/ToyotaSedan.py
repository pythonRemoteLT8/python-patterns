from abstract_factory.Toyota import ToyotaCorolla


class ToyotaCorollaSedan(ToyotaCorolla):
    def get_type(self):
        return "Sedan"

    def get_cylinders_num(self):
        return 4

    def get_engine_volume(self):
        return 1.8

    def get_trunk_size(self):
        return 420