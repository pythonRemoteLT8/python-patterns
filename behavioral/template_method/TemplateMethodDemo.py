from behavioral.template_method.RandomListSortingPerformanceTest import RandomListSortingPerformanceTest
from behavioral.template_method.StringAppendPerformanceTest import StringAppendPerformanceTest


def main():
    test_template = RandomListSortingPerformanceTest()
    test_template.run()

    test_template = StringAppendPerformanceTest()
    test_template.run()


if __name__ == '__main__':
    main()