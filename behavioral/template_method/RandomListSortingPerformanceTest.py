import random

from behavioral.template_method.PerformanceTestTemplate import PerformanceTestTemplate


class RandomListSortingPerformanceTest(PerformanceTestTemplate):
    NUMBERS_NUM = 100000

    def get_warmup_iterations_num(self):
        return 2

    def get_iterations_num(self):
        return 100

    def iteration(self):
        integers = []
        for i in range(RandomListSortingPerformanceTest.NUMBERS_NUM):
            integers.append(random.randint(0, 1000000000))

        integers.sort()