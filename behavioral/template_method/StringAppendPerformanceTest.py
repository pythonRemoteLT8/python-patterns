import random

from behavioral.template_method.PerformanceTestTemplate import PerformanceTestTemplate


class StringAppendPerformanceTest(PerformanceTestTemplate):
    CHARS_NUM = 100000

    def get_warmup_iterations_num(self):
        return 2

    def get_iterations_num(self):
        return 100

    def iteration(self):
        str_ = ''
        for i in range(StringAppendPerformanceTest.CHARS_NUM):
            str_ += chr(random.randint(1, 128))