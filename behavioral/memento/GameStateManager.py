from behavioral.memento.GameStateSnapshot import GameStateSnapshot


class GameStateManager:
    def __init__(self):
        self._snapshots = []

    def save_game(self, game_state):
        self._snapshots.append(GameStateSnapshot(game_state))

    def restore_previous_checkpoint(self):
        return self._snapshots.pop()