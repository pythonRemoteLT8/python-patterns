from behavioral.memento.GameState import GameState
from behavioral.memento.GameStateManager import GameStateManager


def main():
    game_state = GameState(100, 80)

    game_state_manager = GameStateManager()
    game_state_manager.save_game(game_state)
    print(game_state)

    game_state.add_item('Basic Sword')
    game_state.take_damage(10)
    game_state_manager.save_game(game_state)
    print(game_state)

    game_state.take_damage(50)
    game_state.add_item('Shield')
    game_state_manager.save_game(game_state)
    print(game_state)

    game_state_manager.restore_previous_checkpoint()
    game_state_snapshot = game_state_manager.restore_previous_checkpoint()
    game_state.restore_from_snapshot(game_state_snapshot)
    print(game_state)


if __name__ == '__main__':
    main()