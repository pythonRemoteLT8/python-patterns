import copy


class GameStateSnapshot:
    def __init__(self, game_state):
        self._health = game_state.health
        self._mana = game_state.mana
        self._items = copy.deepcopy(game_state.items)

    @property
    def health(self):
        return self._health

    @property
    def mana(self):
        return self._mana

    @property
    def items(self):
        return self._items