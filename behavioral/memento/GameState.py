import copy


class GameState:
    def __init__(self, health=0, mana=0, items=None):
        self._health = health
        self._mana = mana
        if items is None:
            self._items = []
        else:
            self._items = items

    def __str__(self):
        return f"GameState[health={self._health}, mana={self._mana}, items={self._items}]"

    @property
    def health(self):
        return self._health

    @property
    def mana(self):
        return self._mana

    @property
    def items(self):
        return self._items

    def heal(self):
        self._health = 100

    def take_damage(self, damage):
        self._health -= damage

    def add_item(self, item):
        self._items.append(item)

    def lose_all_items(self):
        self._items.clear()

    def restore_from_snapshot(self, snapshot):
        self._health = snapshot.health
        self._mana = snapshot.mana
        self._items = copy.deepcopy(snapshot.items)