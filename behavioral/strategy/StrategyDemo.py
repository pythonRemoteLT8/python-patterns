from behavioral.strategy.SpaceModificationStrategyProvider import SpaceModificationStrategyProvider


def main():
    strategy_type = input("Choose a strategy [DOUBLE|REMOVE|REPLACE]: ")
    inp = "hello from SDA knowledge base!"

    strategy = SpaceModificationStrategyProvider().get(strategy_type)
    output = strategy.modify(inp)

    print(output)


if __name__ == '__main__':
    main()