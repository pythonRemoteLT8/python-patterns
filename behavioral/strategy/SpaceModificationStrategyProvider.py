from behavioral.strategy.DoubleSpacesStrategy import DoubleSpacesStrategy
from behavioral.strategy.RemoveSpacesStrategy import RemoveSpacesStrategy
from behavioral.strategy.ReplaceWithUnderscoreStrategy import ReplaceWithUnderscoreStrategy


class SpaceModificationStrategyProvider:
    def get(self, strategy_type):
        if strategy_type == 'DOUBLE':
            return DoubleSpacesStrategy()
        elif strategy_type == 'REMOVE':
            return RemoveSpacesStrategy()
        elif strategy_type == 'REPLACE':
            return ReplaceWithUnderscoreStrategy()