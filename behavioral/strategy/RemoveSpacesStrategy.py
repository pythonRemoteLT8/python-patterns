from behavioral.strategy.SpacesModificationStrategy import SpacesModificationStrategy


class RemoveSpacesStrategy(SpacesModificationStrategy):
    def modify(self, inp):
        return inp.replace(' ', '')