from behavioral.strategy.SpacesModificationStrategy import SpacesModificationStrategy


class ReplaceWithUnderscoreStrategy(SpacesModificationStrategy):
    def modify(self, inp):
        return inp.replace(' ', '_')