from behavioral.strategy.SpacesModificationStrategy import SpacesModificationStrategy


class DoubleSpacesStrategy(SpacesModificationStrategy):
    def modify(self, inp):
        return inp.replace(' ', '  ')