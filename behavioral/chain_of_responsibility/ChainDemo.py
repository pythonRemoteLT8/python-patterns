from behavioral.chain_of_responsibility.AWSAuthenticationHandler import AWSAuthenticationHandler
from behavioral.chain_of_responsibility.AWSSignature import AWSSignature
from behavioral.chain_of_responsibility.BearerToken import BearerToken
from behavioral.chain_of_responsibility.BearerTokenAuthenticationHandler import BearerTokenAuthenticationHandler
from behavioral.chain_of_responsibility.ChainAuthenticationElement import ChainAuthenticationElement
from behavioral.chain_of_responsibility.UserNameAndPasswordAuthenticationHandler import \
    UserNameAndPasswordAuthenticationHandler
from behavioral.chain_of_responsibility.UserNameAndPasswordCredentials import UserNameAndPasswordCredentials


def main():
    authentication_handler_unp = UserNameAndPasswordAuthenticationHandler()
    authentication_handler_bearer = BearerTokenAuthenticationHandler()
    authentication_handler_aws = AWSAuthenticationHandler()

    last_element = ChainAuthenticationElement(authentication_handler_aws)
    middle_element = ChainAuthenticationElement(authentication_handler_bearer, last_element)
    first_element = ChainAuthenticationElement(authentication_handler_unp, middle_element)

    first_element.authenticate(AWSSignature())
    first_element.authenticate(UserNameAndPasswordCredentials())
    first_element.authenticate(BearerToken())

    middle_element.authenticate(BearerToken())


if __name__ == '__main__':
    main()