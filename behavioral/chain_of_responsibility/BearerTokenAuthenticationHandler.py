import random


from behavioral.chain_of_responsibility.AuthenticationHandler import AuthenticationHandler
from behavioral.chain_of_responsibility.BearerToken import BearerToken


class BearerTokenAuthenticationHandler(AuthenticationHandler):
    def authenticate(self, credentials):
        if self.supports(credentials):
            return self.is_token_valid(credentials)
        else:
            return False

    def supports(self, cls):
        return isinstance(cls, BearerToken)

    def is_token_valid(self, credentials):
        return random.randint(1, 3) == 2