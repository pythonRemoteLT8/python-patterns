class ChainAuthenticationElement:
    def __init__(self, authentication_handler, next = None):
        self._authentication_handler = authentication_handler
        self._next = next

    def authenticate(self, credentials):
        if self._authentication_handler.authenticate(credentials):
            print(f"Authentication using handler {credentials.__class__.__name__}")
            return True
        else:
            return self._next and self._next.authenticate(credentials)