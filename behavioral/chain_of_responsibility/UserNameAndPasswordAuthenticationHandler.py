import random

from behavioral.chain_of_responsibility.AuthenticationHandler import AuthenticationHandler
from behavioral.chain_of_responsibility.UserNameAndPasswordCredentials import UserNameAndPasswordCredentials


class UserNameAndPasswordAuthenticationHandler(AuthenticationHandler):
    def authenticate(self, credentials):
        if self.supports(credentials):
            return self.is_password_valid(credentials)
        else:
            return False

    def supports(self, cls):
        return isinstance(cls, UserNameAndPasswordCredentials)

    def is_password_valid(self, credentials):
        return random.randint(1, 3) == 3