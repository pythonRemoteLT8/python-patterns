import random

from behavioral.chain_of_responsibility.AWSSignature import AWSSignature
from behavioral.chain_of_responsibility.AuthenticationHandler import AuthenticationHandler


class AWSAuthenticationHandler(AuthenticationHandler):
    def authenticate(self, credentials):
        if self.supports(credentials):
            return self.authenticate_in_aws(credentials)
        else:
            return False

    def supports(self, cls):
        return isinstance(cls, AWSSignature)

    def authenticate_in_aws(self, credentials):
        return random.randint(1, 3) == 1