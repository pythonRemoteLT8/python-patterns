from behavioral.Command import Command


class RemoveEmptyLinesCommand(Command):
    def __init__(self, python_file):
        self._python_file = python_file

    def apply(self):
        self._python_file.lines_content = [line for line in self._python_file.lines_content if line.strip()]

    def cancel(self):
        print('Operation not supported!')