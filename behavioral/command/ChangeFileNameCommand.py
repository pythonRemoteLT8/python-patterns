from behavioral.Command import Command


class ChangeFileNameCommand(Command):
    def __init__(self, python_file, new_name):
        self._python_file = python_file
        self._new_name = new_name
        self._previous_name = None

    def apply(self):
        self._previous_name = self._python_file.file_name
        self._python_file.file_name = self._new_name
        print(f"File name changed to: {self._new_name}")

    def cancel(self):
        if self._previous_name:
            self._python_file.file_name = self._previous_name
            self._previous_name = None