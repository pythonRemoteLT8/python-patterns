class PythonFile:
    def __init__(self, file_name, lines_content):
        self.file_name = file_name
        self.lines_content = lines_content

    def add_line(self, line):
        self.lines_content.append(line)

    def __str__(self):
        return f"file_name={self.file_name}\n{self.lines_content}"