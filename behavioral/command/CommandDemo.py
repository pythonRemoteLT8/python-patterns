from behavioral.command.ChangeFileNameCommand import ChangeFileNameCommand
from behavioral.command.PythonFile import PythonFile
from behavioral.command.RemoveEmptyLinesCommand import RemoveEmptyLinesCommand


def main():
    python_file = PythonFile('test.py', ["import this", "    ", "print('That's all folks!)", ""])

    change_file_name_command = ChangeFileNameCommand(python_file, 'zen.py')
    remove_empty_lines_command = RemoveEmptyLinesCommand(python_file)

    change_file_name_command.apply()
    remove_empty_lines_command.apply()

    print(python_file)

    change_file_name_command.cancel()
    remove_empty_lines_command.cancel()

    print(python_file)


if __name__ == '__main__':
    main()