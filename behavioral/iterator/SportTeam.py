import string
from behavioral.iterator.SportTeamIterator import SportTeamIterator


class SportTeam:
    def __init__(self):
        self._members = []

    def add_member(self, name):
        if name.strip():
            self._members.append(string.capwords(name.strip()))

    def __iter__(self):
        return SportTeamIterator(self._members)