from behavioral.iterator.SportTeam import SportTeam


def main():
    sport_team = SportTeam()
    sport_team.add_member(' dudley stokes')
    sport_team.add_member('  devon harris')
    sport_team.add_member('michael white ')
    sport_team.add_member('  chris stokes')

    for member in sport_team:
        print(member)


if __name__ == '__main__':
    main()