import string


class SportTeamIterator:
    def __init__(self, members):
        self._members = members
        self._idx = 0

    def __next__(self):
        if self._idx < len(self._members):
            val = self._members[self._idx]
            self._idx += 1
            return val
        else:
            raise StopIteration

    def __iter__(self):
        return self