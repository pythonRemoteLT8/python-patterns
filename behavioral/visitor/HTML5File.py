from behavioral.visitor.HTMLFile import HTMLFile


class HTML5File(HTMLFile):
    def __init__(self, head, body, visitor):
        self._head = head
        self._body = body
        self._visitor = visitor

    def get_doctype_declaration(self):
        return '<!DOCTYPE html>'

    def get_head(self):
        return self._head

    def get_body(self):
        return self._body

    def accept(self, visitor):
        visitor.validate_html5_file(self)