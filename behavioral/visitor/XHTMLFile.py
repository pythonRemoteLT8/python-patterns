from behavioral.visitor.HTMLFile import HTMLFile


class XHTMLFile(HTMLFile):
    def __init__(self, head, body, visitor):
        self._head = head
        self._body = body
        self._visitor = visitor

    def get_doctype_declaration(self):
        return ('<?xml version="1.0" encoding="UTF-8"?>\n'
                '<!DOCTYPE html\n'
                '        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"\n'
                '        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n'
                '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">')

    def get_head(self):
        return self._head

    def get_body(self):
        return self._body

    def accept(self, visitor):
        visitor.validate_xhtml_file(self)