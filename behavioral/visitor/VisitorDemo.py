from behavioral.visitor.HTML4File import HTML4File
from behavioral.visitor.HTML5File import HTML5File
from behavioral.visitor.HTMLFileValidator import HTMLFileValidator
from behavioral.visitor.XHTMLFile import XHTMLFile


def main():
    visitor = HTMLFileValidator()

    html4_file = HTML4File('<head>\n    <title>Title</title>\n</head>','<body>\n<p>HTML4 FILE</p>\n</body>', visitor)
    html5_file = HTML5File('<head>\n    <meta charset="UTF-8">\n    <title>Title</title>\n</head>'
                           , '<body>\n<p>HTML5 FILE</p>\n</body>',  visitor)
    xhtml_file = XHTMLFile('<head>\n    <title>Title</title>\n</head>', '<body>\n<p>XHTML file</p>\n</body>', visitor)

    html4_file.accept(visitor)
    html5_file.accept(visitor)
    xhtml_file.accept(visitor)


if __name__ == '__main__':
    main()