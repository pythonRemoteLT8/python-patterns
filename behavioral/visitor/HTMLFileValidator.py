from behavioral.visitor.Visitor import Visitor


class HTMLFileValidator(Visitor):
    def validate_html4_file(self, html4_file):
        print("Validating HTML 4 schema with https://validator.w3.org/#validate_by_uri+with_options")

    def validate_html5_file(self, html5_file):
        print("Validating HTML 5 schema with https://validator.w3.org/#validate_by_uri+with_options")

    def validate_xhtml_file(self, xhtml_file):
        print("Validating XHTML schema with https://validator.w3.org/#validate_by_uri+with_options")