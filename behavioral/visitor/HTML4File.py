from behavioral.visitor.HTMLFile import HTMLFile


class HTML4File(HTMLFile):
    def __init__(self, head, body, visitor):
        self._head = head
        self._body = body
        self._visitor = visitor

    def get_doctype_declaration(self):
        return ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"\n'
                '        "http://www.w3.org/TR/html4/loose.dtd">')

    def get_head(self):
        return self._head

    def get_body(self):
        return self._body

    def accept(self, visitor):
        visitor.validate_html4_file(self)