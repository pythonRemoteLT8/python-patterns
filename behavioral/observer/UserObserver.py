from behavioral.observer.BaseObserver import BaseObserver


class UserObserver(BaseObserver):
    def __init__(self, chat_channel, user_name):
        super().__init__(chat_channel)
        self._user_name = user_name
        print(f"User {self._user_name} is joining the {chat_channel.get_name()}")

    def handle_message(self, message, user_type):
        if user_type != 'ADMIN':
            print(f"{self._user_name} sees message {message}")

    def get_observer_type(self):
        return "USER"