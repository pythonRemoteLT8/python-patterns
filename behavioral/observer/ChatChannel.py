class ChatChannel:
    def __init__(self, name):
        self._name = name
        self._observers = []
        self._messages = []

    def subscribe(self, observer):
        if observer not in self._observers:
            self._observers.append(observer)

    def send_message(self, message, observer_type):
        self._messages.append(message)
        self.notify_about_change(message, observer_type)

    def notify_about_change(self, message, observer_type):
        for observer in self._observers:
            observer.handle_message(message, observer_type)

    def get_name(self):
        return self._name