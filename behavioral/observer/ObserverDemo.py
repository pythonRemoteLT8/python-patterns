from behavioral.observer.AdminObserver import AdminObserver
from behavioral.observer.ChatChannel import ChatChannel
from behavioral.observer.UserObserver import UserObserver


def main():
    chat_channel = ChatChannel("design-patterns")

    user_a = UserObserver(chat_channel, "Andrew")
    user_b = UserObserver(chat_channel, "Alice")
    admin_a = AdminObserver(chat_channel, "John")
    admin_b = AdminObserver(chat_channel, "Anna")

    user_a.send_message("Hello all")
    user_b.send_message("Hi Andrew")
    admin_a.send_message("Anna, they can't see what we write")
    admin_b.send_message("Yes, I know")


if __name__ == '__main__':
    main()