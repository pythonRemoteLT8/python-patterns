from behavioral.observer.BaseObserver import BaseObserver


class AdminObserver(BaseObserver):
    def __init__(self, chat_channel, admin_name):
        super().__init__(chat_channel)
        self._admin_name = admin_name
        print(f"{self._admin_name} joined {chat_channel.get_name()} as admin")

    def handle_message(self, message, user_type):
        print(f"{self._admin_name} sees {message} from user whose type is {user_type}")

    def get_observer_type(self):
        return "ADMIN"