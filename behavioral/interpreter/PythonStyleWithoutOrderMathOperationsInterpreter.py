from behavioral.interpreter.Interpreter import Interpreter


class PythonStyleWithoutOrderMathOperationsInterpreter(Interpreter):
    def __init__(self, math_operation_applier):
        self._math_operation_applier = math_operation_applier

    def interpret(self, context):
        split_data = context.strip().split()
        if len(split_data) % 2 == 0:
            raise ArithmeticError()

        value = float(split_data[0])
        for i in range(1, len(split_data), 2):
            value = self._math_operation_applier.apply(split_data[i], value, float(split_data[i+1]))
        return value