from behavioral.interpreter.MathOperationAplier import MathOperationAplier
from behavioral.interpreter.PythonStyleWithoutOrderMathOperationsInterpreter import \
    PythonStyleWithoutOrderMathOperationsInterpreter


def main():
    math_operation_applier = MathOperationAplier()
    interpreter = PythonStyleWithoutOrderMathOperationsInterpreter(math_operation_applier)

    calculation = input('Choose a math operation: ')

    value = interpreter.interpret(calculation)

    print(value)


if __name__ == '__main__':
    main()