class MathOperationAplier:
    def apply(self, math_operation, first, second):
        if math_operation == '+':
            return first + second
        elif math_operation == '-':
            return first - second
        elif math_operation == '*':
            return first * second
        elif math_operation == '/':
            return first / second
        elif math_operation == '**':
            return first ** second
        else:
            raise ArithmeticError()