from behavioral.mediator.Component import Component


class WarningMessage(Component):
    def __init__(self):
        self._mediator = None

    def set_mediator(self, mediator):
        self._mediator = mediator

    def show_warning_message(self):
        print("Are you sure?")

    def hide_warning(self):
        print("")

    def send_request(self, context=None):
        self._mediator.send_info(self, "WarningMessage")