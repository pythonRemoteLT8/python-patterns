from behavioral.mediator.Mediator import Mediator


class UserActionMediator(Mediator):
    def __init__(self, action_applied_message, select_options, warning_message):
        self._action_applied_message = action_applied_message
        self._select_options = select_options
        self._warning_message = warning_message
        self._action_applied_message.set_mediator(self)
        self._select_options.set_mediator(self)
        self._warning_message.set_mediator(self)

    def send_info(self, requester, context):
        if requester == self._action_applied_message:
            self._action_applied_message.display_info()
            self._warning_message.hide_warning()
            self._select_options.hide_options()
        elif requester == self._select_options:
            if context == "load":
                self._select_options.choose_load()
                self._action_applied_message.display_info()
            elif context == "restart":
                self._select_options.choose_restart()
                self._warning_message.show_warning_message()
            elif context == "save":
                self._select_options.choose_save()
                self._action_applied_message.display_info()
            elif context == "displayOptions":
                self._select_options.display_options()
        elif requester == self._warning_message:
            if context == "hide":
                self._warning_message.hide_warning()
            else:
                self._warning_message.show_warning_message()