from behavioral.mediator.Component import Component


class ActionAppliedMessage(Component):
    def __init__(self):
        self._mediator = None

    def set_mediator(self, mediator):
        self._mediator = mediator

    def display_info(self):
        print("Action was applied successfully")

    def send_request(self, context=None):
        self._mediator.send_info(self, "ActionAppliedMessage")