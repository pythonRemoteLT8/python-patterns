from behavioral.mediator.ActionAppliedMessage import ActionAppliedMessage
from behavioral.mediator.SelectOptions import SelectOptions
from behavioral.mediator.UserActionMediator import UserActionMediator
from behavioral.mediator.WarningMessage import WarningMessage


def main():

    action_applied_message = ActionAppliedMessage()
    select_options = SelectOptions()
    warning_message = WarningMessage()

    mediator = UserActionMediator(action_applied_message, select_options, warning_message)

    select_options.send_request()
    select_options.send_request("load")
    select_options.send_request("save")
    select_options.send_request("restart")
    select_options.send_request("hide")


if __name__ == '__main__':
    main()