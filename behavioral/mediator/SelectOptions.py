from behavioral.mediator.Component import Component


class SelectOptions(Component):
    def __init__(self):
        self._mediator = None

    def set_mediator(self, mediator):
        self._mediator = mediator

    def display_options(self):
        print("Options are: Save, Load, Restart")

    def choose_save(self):
        print("Status was saved")

    def choose_load(self):
        print("Loading previous data")

    def choose_restart(self):
        print("Status is restarting")

    def hide_options(self):
        print("Hiding options")

    def send_request(self, context=None):
        self._mediator.send_info(self, context if context else "displayOptions")