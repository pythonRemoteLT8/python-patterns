from behavioral.state.MoneyMachineState import MoneyMachineState
from behavioral.state.ParkingTicketVendingMachineState import ParkingTicketVendingMachineState


class NoPrintingPaperState(ParkingTicketVendingMachineState):
    def __init__(self, machine):
        self._machine = machine

    def move_credit_card_to_sensor(self):
        self._machine.set_message("Cannot pay because there is no printing paper")

    def press_printing_button(self):
        self._machine.set_message("Please call service for additional printing paper")

    def open_machine_and_add_printing_paper_pieces(self):
        self._machine.add_printing_paper_pieces(100)
        self._machine.state = MoneyMachineState.NEED_PAYMENT