from behavioral.state.PaidState import PaidState
from behavioral.state.ParkingTicketVendingMachine import ParkingTicketVendingMachine
from behavioral.state.StillNeedToPayState import StillNeedToPayState


def main():
    machine = ParkingTicketVendingMachine()
    state = StillNeedToPayState(machine)
    state.open_machine_and_add_printing_paper_pieces()
    state.press_printing_button()
    state.move_credit_card_to_sensor()

    state = PaidState(machine)
    state.move_credit_card_to_sensor()
    state.open_machine_and_add_printing_paper_pieces()
    state.press_printing_button()

if __name__ == '__main__':
    main()