import enum
import datetime


class MoneyMachineState(enum.Enum):
    NO_PAPER = 1
    NEED_PAYMENT = 2
    PAID_READY_TO_PRINT = 3
    UNAVAILABLE = 4