from behavioral.state.MoneyMachineState import MoneyMachineState
from behavioral.state.ParkingTicketVendingMachineState import ParkingTicketVendingMachineState


class StillNeedToPayState(ParkingTicketVendingMachineState):
    def __init__(self, machine):
        self._machine = machine

    def move_credit_card_to_sensor(self):
        self._machine.pay_for_one_hour_with_credit_card()
        if self._machine.state == MoneyMachineState.NEED_PAYMENT:
            self._machine.state = MoneyMachineState.PAID_READY_TO_PRINT

    def press_printing_button(self):
        self._machine.set_message("You need to pay first")

    def open_machine_and_add_printing_paper_pieces(self):
        self._machine.set_message("Only authorized personel can add paper")