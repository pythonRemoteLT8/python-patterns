from behavioral.state.MoneyMachineState import MoneyMachineState
from behavioral.state.ParkingTicketVendingMachineState import ParkingTicketVendingMachineState


class PaidState(ParkingTicketVendingMachineState):
    def __init__(self, machine):
        self._machine = machine

    def move_credit_card_to_sensor(self):
        self._machine.set_message("Alreay paid. Press button for printout")

    def press_printing_button(self):
        self._machine.print_ticket()
        if self._machine.get_printing_paper_pieces == 0:
            self._machine.state = MoneyMachineState.NO_PAPER
        else:
            self._machine.state = MoneyMachineState.NEED_PAYMENT

    def open_machine_and_add_printing_paper_pieces(self):
        self._machine.set_message("Only authorized personel can add paper")