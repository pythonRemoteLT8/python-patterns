from behavioral.state.MoneyMachineState import MoneyMachineState
from behavioral.state.ParkingTicketVendingMachineState import ParkingTicketVendingMachineState


class UnavailableState(ParkingTicketVendingMachineState):
    def __init__(self, machine):
        self._machine = machine

    def move_credit_card_to_sensor(self):
        self._machine.set_message("Vending machine unavailable")

    def press_printing_button(self):
        self._machine.go_down()
        self._machine.state = MoneyMachineState.UNAVAILABLE

    def open_machine_and_add_printing_paper_pieces(self):
        self._machine.set_message("Vending machine unavailable")