import datetime

from behavioral.state.MoneyMachineState import MoneyMachineState


class ParkingTicketVendingMachine:
    def __init__(self):
        self._state = MoneyMachineState.NEED_PAYMENT
        self._printing_paper_pieces = 100
        self._message = ''

    def set_message(self, message):
        self._message = message
        print(f"MESSAGE: {message}")

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value

    def get_printing_paper_pieces(self):
        return self._printing_paper_pieces

    def add_printing_paper_pieces(self, pieces):
        self._printing_paper_pieces += pieces
        self._message = "Please pay for the parking"

    def pay_for_one_hour_with_credit_card(self):
        print("Paying $5 for parking")
        self._message = "Please click the button to print the ticket"

    def print_ticket(self):
        self._printing_paper_pieces -= 1
        print(f"Ticket valid thru {datetime.datetime.now() + datetime.timedelta(hours=1)}")
        self._message = "Ticket printed. Please collect it."

    def go_down(self):
        print("Trying to revert last transaction")
        self._message = "Vending machine unavailable"