class Robot:
    def __init__(self):
        self.pos_x = 0.0
        self.pos_y = 0.0

    def __str__(self):
        return f"({self.pos_x}, {self.pos_y})"


class Command:
    def execute(self, robot):
        pass

    def undo(self, robot):
        pass


class MoveUp(Command):
    def __init__(self, steps):
        self._steps = steps

    def execute(self, robot):
        robot.pos_y += self._steps

    def undo(self, robot):
        robot.pos_y -= self._steps


class MoveDown(Command):
    def __init__(self, steps):
        self._steps = steps

    def execute(self, robot):
        robot.pos_y -= self._steps

    def undo(self, robot):
        robot.pos_y += self._steps


class MoveRight(Command):
    def __init__(self, steps):
        self._steps = steps

    def execute(self, robot):
        robot.pos_x += self._steps

    def undo(self, robot):
        robot.pos_x -= self._steps


class MoveLeft(Command):
    def __init__(self, steps):
        self._steps = steps

    def execute(self, robot):
        robot.pos_x -= self._steps

    def undo(self, robot):
        robot.pos_x += self._steps


class Commander:
    def __init__(self, robot):
        self._robot = robot
        self._undo_stack = []
        self._redo_stack = []

    def execute(self, cmd):
        cmd.execute(self._robot)
        self._undo_stack.append(cmd)
        self._redo_stack.clear()

    def undo(self):
        if self._undo_stack:
            cmd = self._undo_stack.pop()
            cmd.undo(self._robot)
            self._redo_stack.append(cmd)

    def redo(self):
        if self._redo_stack:
            cmd = self._redo_stack.pop()
            cmd.execute(self._robot)
            self._undo_stack.append(cmd)


def main():
    robot = Robot()
    commander = Commander(robot)

    while True:
        command = input('Enter command and distance [UDRL] or command [ur]: ')
        if command[0] in 'UDLR':
            robot_cmd, steps = command.split()
            steps = float(steps)
            if robot_cmd == 'U':
                cmd = MoveUp(steps)
            elif robot_cmd == 'D':
                cmd = MoveDown(steps)
            elif robot_cmd == 'R':
                cmd = MoveRight(steps)
            else:
                cmd = MoveLeft(steps)
            commander.execute(cmd)
        elif command[0] == 'u':
            commander.undo()
        elif command[0] == 'r':
            commander.redo()
        else:
            break

        print(robot)


if __name__ == '__main__':
    main()